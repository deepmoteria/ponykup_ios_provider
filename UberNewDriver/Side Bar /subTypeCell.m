//
//  subTypeCell.m
//  Doctor Express Provider
//
//  Created by My Mac on 7/15/15.
//  Copyright (c) 2015 Deep Gami. All rights reserved.
//

#import "subTypeCell.h"

@implementation subTypeCell

- (void)awakeFromNib {
    // Initialization code
    
    self.lblTypeName.font = [UberStyleGuide fontRegular:15.0f];
    self.lblPrice.font = [UberStyleGuide fontRegular:15.0f];

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
